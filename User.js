export default class User{
    constructor({name, email, id}){
        this._name = name;
        this._email = email;
        this._id =id;
    }

    get getName(){
        return this._name;
    }
    
    get getEmail(){
        return this._email;
    }
    get getId(){
        return this._id;
    }
   
}