import User from "./User.js";

export default class Post{
    constructor({body, title, userId, id}){
        this._body = body;
        this._title = title;
        this._userId = userId;
        this._id =id;
    }

    get postBody(){
        return this._body;
    }

    set postBody(value){
        this._body =value;
    }
    
    get postTitle(){
        return this._title;
    }

    set postTitle(value){
         this._title= value;
    }

    get postUserId(){
        return this._userId;
    }

    get postId(){
        return this._id;
    }

    creatPost(){
        const newPost = document.createElement("div");
        newPost.className ="new_post";
        const postTitle = document.createElement("p");
        
        postTitle.innerText = this._title;
        const postText = document.createElement("p");
        postText.innerText = this._body;
        const userInfo = document.createElement("p");
        const btnDelete = document.createElement("button");
        btnDelete.innerText ="Delete Post";
        


        btnDelete.addEventListener("click", () =>{
            
            fetch(`https://ajax.test-danit.com/api/json/posts/${this._id}`,
            {method: "DELETE"
        }).then((res) =>{
            newPost.remove();
        });
        });

       
        fetch("https://ajax.test-danit.com/api/json/users", {
            method:"GET"
        }).then(res=>res.json())
        .then(usersArray=>{
            usersArray.map(user =>{
             const newUser = new User(user)
             if(this._userId === newUser._id){
                userInfo.innerText=`${newUser._name}. ${newUser._email}`
             }
            })
        } )
        newPost.append(postTitle,postText, userInfo, btnDelete);
        return newPost;
    }

    
}

