import Post from "./Post.js";
// import User from "./User";

const app = document.getElementById("app");

const btnAddPost = document.createElement("button");
btnAddPost.innerText = "Add Post";
btnAddPost.className = "btn-add-post";

app.append(btnAddPost);

btnAddPost.addEventListener("click", () => {
  const addBord = document.createElement("div");
  addBord.className = "add-bord";

  const inputTitle = document.createElement("input");
  inputTitle.className = "input-title";

  const inputText = document.createElement("input");
  inputText.className = "input-text";

  const creatBtnNewPost = document.createElement("button");
  creatBtnNewPost.className ="creat-new-post";

  const closeNewPost = document.createElement("button");
  closeNewPost.className ="close-new-post";

  addBord.append(inputTitle, inputText, creatBtnNewPost, closeNewPost);
  app.prepend(addBord);

  closeNewPost.addEventListener("click", () => {
    addBord.remove();
  });


});


fetch("https://ajax.test-danit.com/api/json/posts", {
  method: "GET",
})
  .then((res) => res.json())
  .then((postsArray) => {
    postsArray.map((post) => {
      const newPost = new Post(post);
      app.append(newPost.creatPost());
    });
  });
